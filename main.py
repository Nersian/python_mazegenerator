import algorithms
from point import Point
from maze import MazeGenerator

import time


WIDTH=50
HEIGHT=50

blank_grid = [[Point(x,y) for x in range(0,WIDTH)] for y in range(0,HEIGHT)]

map_grid = algorithms.recursive_backtracking(blank_grid)

maze = MazeGenerator(x_cell=15,y_cell=15, border=3,background_color="white")
maze.create_image(map_grid)
