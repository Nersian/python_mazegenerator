import random
from point import Point


def check_if_visited(y,x,width,height,GRID):
    if 0<=x<height and 0<=y<width:
        if not GRID[x][y].visited:
           return True
    return False

def recursive_backtracking(GRID):
    choice_stack = list()
    stack = list()

    width = len(GRID[0])
    height = len(GRID)
    Current_Point = GRID[random.randint(0,width-1)][random.randint(0,height-1)]
    stack.append(Current_Point)

    while stack:
        choice_stack.clear()
        if check_if_visited(Current_Point.x+1,Current_Point.y,width,height,GRID):
            choice_stack.append(GRID[Current_Point.y][Current_Point.x+1])
        if check_if_visited(Current_Point.x-1,Current_Point.y,width,height,GRID):
            choice_stack.append(GRID[Current_Point.y][Current_Point.x-1])
        if check_if_visited(Current_Point.x,Current_Point.y+1,width,height,GRID):
            choice_stack.append(GRID[Current_Point.y+1][Current_Point.x])
        if check_if_visited(Current_Point.x,Current_Point.y-1,width,height,GRID):
            choice_stack.append(GRID[Current_Point.y-1][Current_Point.x])
        
        Current_Point.visited = True

        if choice_stack:
            Random_Point = random.choice(choice_stack)
            Point.open_path(Current_Point, Random_Point)
            Current_Point = Random_Point
            stack.append(Current_Point)
        else:
            Current_Point = stack.pop()

    return GRID