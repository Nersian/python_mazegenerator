from PIL import Image
from point import Point

class MazeGenerator:
    def __init__(self,x_cell=30, y_cell=30, border=4,border_color="black", background_color="white"):
        self.x_cell = x_cell
        self.y_cell = y_cell
        self.border = border
        self.border_color = border_color
        self.background_color = background_color

    def create_cell(self, Point):
        cell = Image.new('RGB', (self.x_cell, self.y_cell), self.background_color)
        wall_horizontal = Image.new('RGB', (self.border, self.y_cell), self.border_color)
        wall_vertical = Image.new('RGB', (self.x_cell, self.border), self.border_color)

        #Add top
        top = Image.new('RGB', (self.border, self.border), self.border_color)
        cell.paste(top,(0,0,self.border,self.border)) #Top Left
        cell.paste(top,(self.x_cell-self.border,self.y_cell-self.border,self.x_cell,self.y_cell)) #Bottom right
        cell.paste(top,(self.x_cell-self.border,0,self.x_cell,self.border)) #Top Right
        cell.paste(top,(0,self.y_cell-self.border,self.border,self.y_cell)) #Bottom Left

        if not Point.left:
            cell.paste(wall_horizontal,(0,0,self.border,self.y_cell))
        if not Point.right:
            cell.paste(wall_horizontal,(self.x_cell-self.border,0,self.x_cell,self.y_cell))
        if not Point.up:
            cell.paste(wall_vertical,(0,0,self.x_cell,self.border))
        if not Point.down:
            cell.paste(wall_vertical,(0,self.y_cell-self.border,self.x_cell,self.y_cell))

        return cell

    def create_image(self, grid):
        maze = Image.new('RGB', (self.x_cell*len(grid[0])+2*self.border, self.y_cell*len(grid)+2*self.border))
        for i, row in enumerate(grid):
            for j, cell in enumerate(row):
                new_cell = self.create_cell(cell)
                maze.paste(new_cell, (j*self.x_cell+self.border, i*self.y_cell+self.border, (j+1)*self.x_cell+self.border, (i+1)*self.y_cell+self.border))
    
        maze.show()
    
