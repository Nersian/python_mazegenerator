class Point():
    def __init__(self,x,y):
        self.x = x
        self.y = y
        self.left = False
        self.right = False
        self.up = False
        self.down = False
        self.visited = False

    @staticmethod
    def open_path(Point1, Point2):
        if Point1.x > Point2.x:
            Point1.left = Point2.right = True
        if Point1.x < Point2.x:
            Point1.right = Point2.left = True
        if Point1.y > Point2.y:
            Point1.up = Point2.down = True
        if Point1.y < Point2.y:
            Point1.down = Point2.up = True
